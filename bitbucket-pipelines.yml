image:
  name: python:3.7

setup: &setup
  step:
    name: Setup testing resources
    script:
      - STACK_NAME="aws-cloudformation-deploy-ci-${BITBUCKET_BUILD_NUMBER}"
      - S3_BUCKET="${STACK_NAME}-bucket"
      - pipe: atlassian/aws-cloudformation-deploy:0.4.3
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          STACK_NAME: ${STACK_NAME}
          TEMPLATE: "./test/S3BucketCloudFormationStackTemplate.yml"
          CAPABILITIES: ['CAPABILITY_IAM']
          WAIT: 'true'
          STACK_PARAMETERS: >
              [{
                "ParameterKey": "BucketName",
                "ParameterValue": "${S3_BUCKET}"
              }]
      - pip install awscli==1.16.174
      - aws s3 cp --recursive test/templates s3://${S3_BUCKET}

release-dev: &release-dev
  step:
    name: Release development version
    trigger: manual
    script:
      - pip install semversioner
      - VERSION=$(semversioner current-version).${BITBUCKET_BUILD_NUMBER}-dev
      - pipe: atlassian/bitbucket-pipe-release:4.0.1
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/$BITBUCKET_REPO_SLUG
          GIT_PUSH: 'false'
          VERSION: ${VERSION}
    services:
      - docker

test: &test
  parallel:
    - step:
        name: Test
        oidc: true
        image: python:3.7
        script:
          - pip install -r requirements.txt
          - pip install -r test/requirements.txt
          - flake8 --ignore E501,E125
          - STACK_NAME="aws-cloudformation-deploy-ci-${BITBUCKET_BUILD_NUMBER}"
          - export S3_BUCKET="${STACK_NAME}-bucket"
          - pytest -p no:cacheprovider test/test_unit.py --capture=no --verbose
          - pytest test/test.py --verbose --capture=no
        after-script:
          - STACK_NAME="aws-cloudformation-deploy-ci-${BITBUCKET_BUILD_NUMBER}"
          - S3_BUCKET="${STACK_NAME}-bucket"
          - aws s3 rm s3://${S3_BUCKET} --recursive
          - aws --region "${AWS_DEFAULT_REGION}" cloudformation delete-stack --stack-name ${STACK_NAME}
          - aws --region "${AWS_DEFAULT_REGION}" cloudformation wait stack-delete-complete --stack-name ${STACK_NAME}
        services:
        - docker
    - step:
        name: Lint the Dockerfile
        image: hadolint/hadolint:latest-debian
        script:
          - hadolint Dockerfile

push: &push
  step:
    name: Push and Tag
    script:
      - pip install semversioner==0.12.0
      - current_version=$(semversioner current-version)
      - new_version=$(semversioner status | grep "Next version" | grep -o "[0-9\.]*")
      - sed -i "s/version='$current_version'/version='$new_version'/g" setup.py
      - pipe: atlassian/bitbucket-pipe-release:4.0.1
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/$BITBUCKET_REPO_SLUG
    services:
      - docker


pipelines:
  default:
    - <<: *setup
    - <<: *test
    - <<: *release-dev
  branches:
    master:
      - <<: *setup
      - <<: *test
      - <<: *push
