pytest==4.5.0
pytest-ordering==0.6
flake8==3.7.7

awscli==1.18.188

bitbucket-pipes-toolkit==1.14.2